<link rel="stylesheet" href="../json_crud/bootstrap/css/bootstrap.min.css" />  
  
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title> table tickets</title>

</head>
<body>
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
		 <div id="collapse4" class="body">
                <div class="table-responsive">
                    
                        

                <a href = "add.php"  class = " btn btn-primary btn-lg " title = "Ajouter" style="margin:5px 0px;"><b> Ajouter un ticket</b></a>



<table id="ticket_table" class="table  table-bordered table-condensed  table-striped "  style="width:70%">
	<thead>
		<tr>
		<th>ID</th>
		
		<th>Date</th>
		<th>Description</th>
		<th>Sévérité</th>
		<th>Action</th>
		 </tr>
	</thead>
	<tbody>
		<?php
			//fetch data from json
			$data = file_get_contents('members.json');
			//decode into php array
			$data = json_decode($data);

			$index = 0; $id=1;
			foreach($data as $row){
				echo "
					<tr>
						<td>".$id."</td>
						
						<td>".$row->date."</td>
						<td>".$row->description."</td>
						<td>".$row->severite."</td>
						<td>
							<a href='edit.php?index=".$index."' class = 'btn btn-warning btn-lg'>Modifier</a>
							<a href='delete.php?index=".$index."' class = 'btn btn-danger btn-lg'>Supprimer</a>
						</td>
					</tr>
				";
$id++;
				$index++;
			}
		?>
	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</body>
</html>
<script src="../json_crud/bootstrap/jquery.js"></script>

<script src="../json_crud/bootstrap/js/bootstrap.min.js"></script>



