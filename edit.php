<link rel="stylesheet" href="../json_crud/bootstrap/css/bootstrap.min.css" />  

<?php
	$index = $_GET['index'];

	//get json data
	$data = file_get_contents('members.json');
	$data_array = json_decode($data);

	$row = $data_array[$index];

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>CRUD Operation on JSON File using PHP</title>
</head>
<body>
<form method="POST">
		
	<br> <br>
	<div class="form-group col-lg-6">
	
	
	<div class="form-group">
		<label for="date">Date</label>
		<input type="date" class="form-control" id="date" name="date" value="<?php echo $row->date; ?>" required>
	</div>

	<div class="form-group">
		<label for="description">Description</label>
		<textarea type="text" class="form-control" rows="5" cols="10" id="description" rows="6"  name="description" required 
		value="<?php echo $row->description; ?>"><?php echo $row->description; ?></textarea>

	</div>

	<div class="form-group">
<label for="severite">Sévérité</label>
<br>
<input type="radio"  name="severite" value="Urgent" />Urgent<br />
<input type="radio"  name="severite" value="Normal" />Normal<br />
<input type="radio"   checked="checked" name="severite" value="Bas" />Bas<br />
	</div>

	<input type="submit" class="btn btn-success btn-sm" name="save" value="Save">
		<a href="index.php" class="btn btn-info btn-sm">Retour</a>


	</div>
</form>

<?php
	if(isset($_POST['save'])){
		$input = array(
			
			
			'date' => $_POST['date'],
			'description' => $_POST['description'],
			'severite' => $_POST['severite']
		);

		$data_array[$index] = $input;

		$data = json_encode($data_array, JSON_PRETTY_PRINT);
		file_put_contents('members.json', $data);

		header('location: index.php');
	}
?>
</body>
</html>
<script src="../json_crud/bootstrap/jquery.js"></script>

<script src="../json_crud/bootstrap/js/bootstrap.min.js"></script>



