
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="../json_crud/bootstrap/css/bootstrap.min.css" />  
	<script src="../json_crud/bootstrap/jquery.js"></script>
    <script src="../json_crud/bootstrap/js/bootstrap.min.js"></script>


	<meta charset="utf-8">
	<title>Ajout d'un ticket</title>

</head>
<body>
<form method="POST">
	
	
	<br> <br>
	<div class="form-group col-lg-6">
	
	
	<div class="form-group">
		<label for="date">Date</label>
		<input type="Date" class="form-control" id="date" name="date" required>
	</div>
	


	<div class="form-group">
  <label for="description">Description:</label>

  <textarea class="form-control" rows="5" cols="10" id="description" name="description" required></textarea>

</div>





	<div class="form-group">
		<label for="severite">Sévérité</label>
		<br>
<input type="radio"  name="severite" value="Urgent" />Urgent<br />
<input type="radio"  name="severite" value="Normal" />Normal<br />
<input type="radio"  checked="checked"  name="severite" value="Bas" />Bas<br />
</div>



	<input type="submit" class = " btn btn-success btn-sm "  name="save" value="Save">
<a href="index.php" class = " btn btn-info btn-sm ">Retour</a>
</div>
</form>

<?php
	if(isset($_POST['save'])){
		//open the json file
		$data = file_get_contents('members.json');
		$data = json_decode($data, true);


		//data in out POST
		$input = array(
			
			
			'date' => $_POST['date'],
			'description' => $_POST['description'],
			'severite' => $_POST['severite']
		);

		//append the input to our array
		$data[] = $input;
		//encode back to json
		$data = json_encode($data, JSON_PRETTY_PRINT);
		file_put_contents('members.json', $data);

		header('location: index.php');
	}
?>
</body>
</html>


